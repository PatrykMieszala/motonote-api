//
//  main.swift
//  MotoNoteAPI
//

import PerfectLib
import PerfectHTTP
import PerfectHTTPServer

let server = HTTPServer()
server.serverAddress = "localhost"
server.serverName = "MotoNote API Develop"
server.documentRoot = "./webroot"
server.serverPort = 8080

let routes = MainRoutes.mainRoutes()
server.addRoutes(routes)

MainRegister.register()

do {
    try server.start()
} catch {
	fatalError("\(error)") // fatal error launching server
}

