//
//  Model.swift
//  MotoNoteAPI
//
//  Created by Patryk on 01.07.2017.
//
//

import PerfectLib

class Model: JSONConvertibleObject {
    
    static var registerName: String {
        return String(describing: self)
    }
}
