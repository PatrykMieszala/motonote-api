//
//  Handler.swift
//  MotoNoteAPI
//
//  Created by Patryk on 01.07.2017.
//
//

import PerfectLib
import PerfectHTTP

protocol Handler { }

extension Handler {
    
    static func map<T: Model>(body: String?) throws -> T {
        do {
            var JSON = try body?.jsonDecode() as? [String: Any]
            JSON?[JSONDecoding.objectIdentifierKey] = T.registerName
            
            let encoded = try JSON.jsonEncodedString()
            
            if let model = try encoded.jsonDecode() as? T {
                return model
            } else {
                throw HandlerError.cantDecode
            }
        }
        catch let error {
            throw error
        }
    }
    
    static func map<T: Model>(body: String?) throws -> [T] {
        do {
            if let objects = try body?.jsonDecode() as? [T] {
                return objects
            }
            
            guard var JSON = try body?.jsonDecode() as? [[String: Any]] else {
                throw HandlerError.cantDecode
            }
            for (index, var object) in JSON.enumerated() {
                object[JSONDecoding.objectIdentifierKey] = T.registerName
                JSON[index] = object
            }
            
            let encoded = try JSON.jsonEncodedString()
            
            if let model = try encoded.jsonDecode() as? [T] {
                return model
            } else {
                throw HandlerError.mappingError
            }
        }
        catch let error {
            throw error
        }
    }
}

enum HandlerError: Error {
    case mappingError
    case cantDecode
}
