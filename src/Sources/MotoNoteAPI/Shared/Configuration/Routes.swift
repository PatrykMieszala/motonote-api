//
//  Routes.swift
//  MotoNoteAPI
//
//  Created by Patryk on 01.07.2017.
//
//

import PerfectHTTP

class MainRoutes {
    
    static func mainRoutes() -> Routes {
        
        var routes = Routes()
        
        // Route for Vehicles
        routes.add(VehiclesRoutes.routes())
        
        // Route for Vehicle
        routes.add(VehicleRoutes.routes())
        
        return routes
    }
}
