//
//  VehicleRoutes.swift
//  MotoNoteAPI
//
//  Created by Patryk on 01.07.2017.
//
//

import PerfectHTTP

class VehicleRoutes {
    
    static func routes() -> Routes {
        var routes = Routes(baseUri: "/vehicle")
        
        //POST Handler for Vehicles
        routes.add(method: .post, uri: "", handler: VehicleHandler.vehiclePOST)
        
        return routes
    }
}
