//
//  VehicleHandler.swift
//  MotoNoteAPI
//
//  Created by Patryk on 01.07.2017.
//
//

import PerfectHTTP
import PerfectLib
import Foundation

class VehicleHandler: Handler {
    
    // POST Vehicle handler
    static func vehiclePOST(request: HTTPRequest, _ response: HTTPResponse) {
        do {
            let vehicle: VehicleModel = try map(body: request.postBodyString)
            vehicle.id = NSUUID().uuidString
            
            let vehiclesFile = File("Resources/Vehicles.json")
            try vehiclesFile.open(File.OpenMode.readWrite, permissions: File.PermissionMode.rwxUser)
            let vehiclesJSONString = try vehiclesFile.readString()
            var vehicles: [VehicleModel] = try map(body: vehiclesJSONString)
            vehicles.append(vehicle)
            
            let temp = TemporaryFile(withPrefix: "Vehicles")
            try temp.write(string: try vehicles.jsonEncodedString())
            
            try temp.copyTo(path: "Resources/Vehicles.json", overWrite: true)
            
            temp.delete()
            
            try response.setBody(json: vehicle, skipContentType: false)
        }
        catch let error {
            Log.error(message: "\(error)")
            response.setBody(string: "\(error)")
        }
        response.completed()
    }
    
}
