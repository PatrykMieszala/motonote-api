//
//  VehicleRegister.swift
//  MotoNoteAPI
//
//  Created by Patryk on 01.07.2017.
//
//

import PerfectLib

class VehicleRegister {
    
    class func register() {
        Log.info(message: "VehicleModel Register")
        JSONDecoding.registerJSONDecodable(name: VehicleModel.registerName, creator: { return VehicleModel ()
        })
    }
}
