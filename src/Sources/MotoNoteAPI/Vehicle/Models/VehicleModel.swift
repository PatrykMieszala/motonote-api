//
//  VehicleModel.swift
//  MotoNoteAPI
//
//  Created by Patryk on 01.07.2017.
//
//

import PerfectLib

enum VehicleType: String, JSONConvertible {
    case car = "car"
    case motocycle = "motocycle"
}

extension VehicleType {
    func jsonEncodedString() throws -> String {
        return self.rawValue
    }
}

class VehicleModel: Model {
    
    // MARK: - Public Properties
    var id: String = ""
    var type: VehicleType = .car
    var brand: String = ""
    var model: String = ""
    var mileage: Double = 0
    
    override func setJSONValues(_ values: [String : Any]) {
        self.id = getJSONValue(named: "id", from: values, defaultValue: "")
        self.type = getJSONValue(named: "type", from: values, defaultValue: .car)
        self.brand = getJSONValue(named: "brand", from: values, defaultValue: "")
        self.model = getJSONValue(named: "model", from: values, defaultValue: "")
        self.mileage = getJSONValue(named: "mileage", from: values, defaultValue: 0)
    }
    
    override func getJSONValues() -> [String : Any] {
        return [
            JSONDecoding.objectIdentifierKey: VehicleModel.registerName,
            "id": id,
            "type": type.rawValue,
            "brand": brand,
            "model": model,
            "mileage": mileage
        ]
    }
    
}
