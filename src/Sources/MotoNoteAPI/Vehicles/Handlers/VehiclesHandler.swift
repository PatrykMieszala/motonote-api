//
//  VehiclesHandler.swift
//  MotoNoteAPI
//
//  Created by Patryk on 01.07.2017.
//
//

import PerfectHTTP
import PerfectLib

class VehiclesHandler {
    
    // GET Vehicles handler
    static func vehiclesGET(request: HTTPRequest, _ response: HTTPResponse) {
        do {
            let vehiclesTemplate = File("Resources/Vehicles.json")
            try vehiclesTemplate.open(File.OpenMode.read, permissions: File.PermissionMode.readUser)
            let vehiclesJSONString = try vehiclesTemplate.readString()
            let vehiclesJSON = try vehiclesJSONString.jsonDecode()
            try response.setBody(json: vehiclesJSON)
        }
        catch let error {
            Log.error(message: "\(error)")
        }
        response.completed()
    }
}
