//
//  VehiclesRoutes.swift
//  MotoNoteAPI
//
//  Created by Patryk on 01.07.2017.
//
//

import PerfectHTTP

class VehiclesRoutes {
    
    static func routes() -> Routes {
        var routes = Routes(baseUri: "/vehicles")
        
        //GET Handler for Vehicles
        routes.add(method: .get, uri: "", handler: VehiclesHandler.vehiclesGET)
        
        return routes
    }
}
